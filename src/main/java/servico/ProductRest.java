package servico;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import model.Image;
import model.Product;
import persistence.ImageDao;
import persistence.ProductDao;

@Path("/product")
public class ProductRest {

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProduct(){
		try{
			ProductDao pDao = new ProductDao();
			List<Product> lista = pDao.findAll();
			
			List<Product> nvLista = new ArrayList<Product>();
			
			for(Product p : lista){
				Product pp = new Product(p.getId(), p.getName(), 
						p.getDescription(), null, null);
				nvLista.add(pp);
			}
			
			return Response.ok(new Gson().toJson(nvLista)).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/find/{pid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductById(@PathParam("pid") String id){
		try{
			ProductDao pDao = new ProductDao();
			Product product = pDao.findByOne(Integer.parseInt(id));
			Product pp  = null;
			if(product != null){			
				pp = new Product(product.getId(), product.getName(), 
						product.getDescription(), null, null);
			}else{
				
			}
			return Response.ok(new Gson().toJson(pp)).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/relations/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProductRelations(){
		try{
			ProductDao pDao = new ProductDao();
			List<Product> lista = pDao.getAllRelations();
			
			List<Product> nvLista = new ArrayList<Product>();
			
			for(Product p : lista){
				Product chield = null;
				if(p.getProduct() != null){
					chield = new Product(p.getProduct().getId(), p.getProduct().getName(), 
							p.getProduct().getDescription(), null, null);
				}
				
				List<Image> images = new ArrayList<Image>();
				
				if(p.getImages().size() > 0){
					for(Image img : p.getImages()){
						Image im = new Image(img.getId(), img.getType(), null);
						images.add(im);
					}
				}
				
				Product pp = new Product(p.getId(), p.getName(), 
						p.getDescription(), chield, images);
				
				nvLista.add(pp);
			}
			
			return Response.ok(new Gson().toJson(nvLista)).build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/relations/find/{pid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductByIdRelations(@PathParam("pid") String id){
		try{
			ProductDao pDao = new ProductDao();
			
			Product product = pDao.findByOne(Integer.parseInt(id));
			
			Product chield = null;
			if(product.getProduct() != null){
				chield = new Product(product.getProduct().getId(), product.getProduct().getName(), 
						product.getProduct().getDescription(), null, null);
			}
			
			List<Image> images = new ArrayList<Image>();
			
			if(product.getImages().size() > 0){
				for(Image img : product.getImages()){
					Image im = new Image(img.getId(), img.getType(), null);
					images.add(im);
				}
			}
			
			Product pp = new Product(product.getId(), product.getName(), 
						product.getDescription(), chield, images);
			
			return Response.ok(new Gson().toJson(pp)).build();
			

		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/child/{pid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChildOfProduct(@PathParam("pid") String id){
		try{
			ProductDao pDao = new ProductDao();
			
			List<Product> lista = pDao.getChildOfProduct(Integer.parseInt(id));
			List<Product> nvLista = new ArrayList<Product>();
			
			for(Product p : lista){
				
				Product pp = new Product(p.getId(), p.getName(), 
						p.getDescription(), null, null);
				
				nvLista.add(pp);
			}
			
			return Response.ok(new Gson().toJson(nvLista)).build();
			

		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/images/{pid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImagesOfProduct(@PathParam("pid") String id){
		try{
			ImageDao imgDao = new ImageDao();
			
			List<Image> lista = imgDao.getImageOfProduct(Integer.parseInt(id));
			List<Image> nvLista = new ArrayList<Image>();
			
			for(Image im : lista){
				
				Image image = new Image(im.getId(), im.getType(), null);
				nvLista.add(image);
			}
			
			return Response.ok(new Gson().toJson(nvLista)).build();
			

		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("/init")
	@Produces(MediaType.APPLICATION_JSON)
	public Response init(){
		try{
			
			ProductDao pDao = new ProductDao();
			
			Product p1 = new Product(null, "Teste", "Testando", null, null);
			
			Image im1 = new Image(null, "Tipo 1", p1);
			Image im2 = new Image(null, "Tipo 2", p1);
			
			List<Image> lista1 = new ArrayList<Image>();
			lista1.add(im1);
			lista1.add(im2);
			
			p1.setImages(lista1);
			
			pDao.create(p1);
			
			Product p2 = new Product(null, "Teste 2", "Testando 2", null, null);
			pDao.create(p2);
			
			Product p3 = new Product(null, "Teste 3", "Testando 3", p2, null);
			pDao.create(p3);
			
			Product p4 = new Product(null, "Teste 4", "Testando 4", p2, null);
			pDao.create(p4);
			
			Product p5 = new Product(null, "Teste 5", "Testando 5", p1, null);
			pDao.create(p5);
			
			System.out.println("Product created");
			
			for(Product p : pDao.findAll()){
				System.out.println(p.getName());
				System.out.println(p.getDescription());
				if(p.getProduct() != null){
					System.out.println(p.getProduct().getName());
				}
				if(p.getImages().size() > 0){
					for(Image i : p.getImages()){
						System.out.println(i.getType());
					}
				}
				System.out.println("----------------");
			}
			return Response.ok().build();
		}catch(Exception e){
			e.printStackTrace();
			return Response.status(500).build();
		}
		
	}
	
}
