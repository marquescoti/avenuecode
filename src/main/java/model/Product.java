package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@XmlRootElement
@Entity
public class Product {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer id;
	@Column
	private String name;
	@Column
	private String description;
	
	@ManyToOne
    @JoinColumn(name = "parent_product_id")
	private Product product;
	
	@OneToMany(mappedBy="product", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Image> images;
	
	@OneToMany
	@Fetch(FetchMode.SELECT)  
	private List<Product> chieldProduct;
	
	public Product(Integer id, String name, String description,
			Product product, List<Image> images) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.product = product;
		this.images = images;
	}
	public Product() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public List<Product> getChieldProduct() {
		return chieldProduct;
	}
	public void setChieldProduct(List<Product> chieldProduct) {
		this.chieldProduct = chieldProduct;
	}
	
	
	
}
