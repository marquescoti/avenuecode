package persistence;

import java.util.ArrayList;
import java.util.List;

import model.Image;
import model.Product;

public class Teste {

	public static void main(String[] args) {
		
		try{
			
			ProductDao pDao = new ProductDao();
			
			Product p1 = new Product(null, "Teste", "Testando", null, null);
			
			Image im1 = new Image(null, "Tipo 1", p1);
			Image im2 = new Image(null, "Tipo 2", p1);
			
			List<Image> lista1 = new ArrayList<Image>();
			lista1.add(im1);
			lista1.add(im2);
			
			p1.setImages(lista1);
			
			pDao.create(p1);
			
			Product p2 = new Product(null, "Teste 2", "Testando 2", null, null);
			pDao.create(p2);
			
			Product p3 = new Product(null, "Teste 3", "Testando 3", p2, null);
			pDao.create(p3);
			
			Product p4 = new Product(null, "Teste 4", "Testando 4", p2, null);
			pDao.create(p4);
			
			Product p5 = new Product(null, "Teste 5", "Testando 5", p1, null);
			pDao.create(p5);
			
			System.out.println("Product created");
			
			for(Product p : pDao.findAll()){
				System.out.println(p.getName());
				System.out.println(p.getDescription());
				if(p.getProduct() != null){
					System.out.println(p.getProduct().getName());
				}
				if(p.getImages().size() > 0){
					for(Image i : p.getImages()){
						System.out.println(i.getType());
					}
				}
				System.out.println("----------------");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}
