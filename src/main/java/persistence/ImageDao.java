package persistence;

import java.util.List;

import model.Image;

public class ImageDao extends GenericDao<Image, Integer> {

	public ImageDao() {
		super(new Image());
	}

	public List<Image> getImageOfProduct(Integer id)throws Exception{
		s = HibernateUtil.getSessionFactory().openSession();
		q = s.createQuery("from Image i where i.product.id = :pid");
		q.setParameter("pid", id);
		List<Image> lista = q.list();
		s.close();
		return lista;
	}
	
}
