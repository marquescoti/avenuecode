package persistence;

import java.util.List;

import org.hibernate.Criteria;

import model.Product;

public class ProductDao extends GenericDao<Product, Integer> {

	public ProductDao() {
		super(new Product());
	}

	public List<Product> getAllRelations()throws Exception{
		s = HibernateUtil.getSessionFactory().openSession();
		q = s.createQuery("from Product p");
		List<Product> lista = q.list();
		//s.close();
		return lista;
	}
	
	public List<Product> getChildOfProduct(Integer id)throws Exception{
		s = HibernateUtil.getSessionFactory().openSession();
		q = s.createQuery("from Product p where p.product.id = :pid");
		q.setParameter("pid", id);
		List<Product> lista = q.list();
		s.close();
		return lista;
	}
	
}
