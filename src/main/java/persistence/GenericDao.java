package persistence;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class GenericDao<E, ID extends Serializable> implements IDao<E, ID> {

	Session s;
	Transaction t;
	Query q;
	Criteria c;
	
	E entity;
	
	public GenericDao(E entity) {
		this.entity = entity;
	}
	
	@Override
	public void create(E obj) throws Exception {
		s = HibernateUtil.getSessionFactory().openSession();
		t = s.beginTransaction();
		s.save(obj);
		t.commit();
		s.close();
		
	}

	@Override
	public void update(E obj) throws Exception {
		s = HibernateUtil.getSessionFactory().openSession();
		t = s.beginTransaction();
		s.update(obj);
		t.commit();
		s.close();
	}

	@Override
	public void delete(E obj) throws Exception {
		s = HibernateUtil.getSessionFactory().openSession();
		t = s.beginTransaction();
		s.delete(obj);
		t.commit();
		s.close();
	}

	@Override
	public E findByOne(ID id) throws Exception {
		s = HibernateUtil.getSessionFactory().openSession();
		E obj = (E) s.get(entity.getClass(), id);
		s.close();
		return obj;
	}

	@Override
	public List<E> findAll() throws Exception {
		s = HibernateUtil.getSessionFactory().openSession();
		List<E> list = (List<E>) s.createCriteria(entity.getClass()).list();
		s.close();
		return list;
	}

	
	
}
