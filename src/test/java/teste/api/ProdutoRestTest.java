package teste.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import model.Image;
import model.Product;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import persistence.ImageDao;
import persistence.ProductDao;

public class ProdutoRestTest {

	ProductDao pDao;
	ImageDao iDao;
	
	@Before
	public void iniciar(){
		pDao = new ProductDao();
		iDao = new ImageDao();
		
		try{
			
			Product p1 = new Product(null, "Teste", "Testando", null, null);
			
			Image im1 = new Image(null, "Tipo 1", p1);
			Image im2 = new Image(null, "Tipo 2", p1);
			
			List<Image> lista1 = new ArrayList<Image>();
			lista1.add(im1);
			lista1.add(im2);
			
			p1.setImages(lista1);
			
			pDao.create(p1);
			
			Product p2 = new Product(null, "Teste 2", "Testando 2", null, null);
			pDao.create(p2);
			
			Product p3 = new Product(null, "Teste 3", "Testando 3", p2, null);
			pDao.create(p3);
			
			Product p4 = new Product(null, "Teste 4", "Testando 4", p2, null);
			pDao.create(p4);
			
			Product p5 = new Product(null, "Teste 5", "Testando 5", p1, null);
			pDao.create(p5);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testExistProduct() throws Exception {
		Assert.assertTrue( pDao.findAll().size() > 0 );
	}
	
	@Test
	public void testFindId()throws Exception {
		Assert.assertEquals(pDao.findByOne(1).getName(), "Teste");
	}
	
}
