Nome do desenvolvedor: Rodrigo Marques
E-mail: marques.coti@gmail.com
https://marquescoti@bitbucket.org/marquescoti/avenuecode.git


Antes de realizar os processos abaixo, devemos ter a certeza que o maven esta instalado e configurado na maquina, realizar um clone do projeto no bitbucked e pelo terminal baixar as dependencias do maven:
mvn install

* Banco ja foi criado na raiz do projeto

Execute esta URL para realizar o cadastro de alguns produtos como teste
http://localhost:8888/projeto/web/product/init

Opera��es realizadas pela API
a) Get all products excluding relationships (child products, images)
http://localhost:8888/projeto/web/product/all

b) Get all products including specified relationships (child product and/or images)
http://localhost:8888/projeto/web/product/relations/all

c) Same as 1 using specific product identity
http://localhost:8888/projeto/web/product/find/1

d) Same as 2 using specific product identity
http://localhost:8888/projeto/web/product/relations/find/1

e) Get set of child products for specific product
http://localhost:8888/projeto/web/product/child/1

f) Get set of images for specific product
http://localhost:8888/projeto/web/product/images/1



Como executar o Projeto
1. Para executar os testes basta acessar um terminal
2. Navegar ate a pasta inicial do projeto, por exemplo: 
	cd c:/projeto
3. Execute o comando:
	mvn -Djetty.port=8888 jetty:run

- Como executar os testes automatizados

1. Para executar os testes basta acessar um terminal
2. Navegar ate a pasta inicial do projeto, por exemplo: 
	cd c:/projeto
3. Executar no console mvn test 